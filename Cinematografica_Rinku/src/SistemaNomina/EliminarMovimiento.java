package SistemaNomina;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EliminarMovimiento extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;

	private JLabel lblNmeroDeEmpleado;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblFecha;
	private JLabel lblTipo;

	private JCheckBox chckbxCantidadDeEntregas;
	private JCheckBox chckbxCubriTurno;

	private JSpinner spinner;

	private JButton btnOk;
	private JButton btnCancel;
	private JComboBox comboBox;
	private JButton btnBuscar_1;
	private JScrollPane scrollPane_1;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contFech;
	public static String fechasistema;
	public static int contAce;

	public static int cubrioTurno = 0;
	public static String turnoCub = "";
	public static int cantEntregas = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarMovimiento frame = new EliminarMovimiento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarMovimiento() {
		setTitle("Eliminar Movimientos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 584, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CapturaMovimientos EliminarMovimiento = new CapturaMovimientos();
				EliminarMovimiento.setVisible(true);
				dispose();
			}
		});
		btnNuevo.setBounds(27, 24, 83, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarMovimientos EliminarMovimiento = new BuscarMovimientos();
				EliminarMovimiento.setVisible(true);
				dispose();
			}
		});
		btnBuscar.setBounds(112, 24, 89, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarMovimientos EliminarMovimiento = new ModificarMovimientos();
				EliminarMovimiento.setVisible(true);
				dispose();
			}
		});
		btnModificar.setBounds(204, 24, 89, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(295, 24, 83, 23);
		contentPane.add(btnEliminar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 531, 50);
		contentPane.add(scrollPane);

		lblNmeroDeEmpleado = new JLabel("N\u00FAmero de empleado:");
		lblNmeroDeEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmeroDeEmpleado.setBounds(20, 58, 132, 50);
		contentPane.add(lblNmeroDeEmpleado);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(52, 118, 50, 50);
		contentPane.add(lblNombre);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(52, 158, 42, 56);
		contentPane.add(lblRol);

		lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setBounds(275, 58, 47, 50);
		contentPane.add(lblFecha);

		textField = new JTextField();
		textField.setBounds(151, 70, 114, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(112, 130, 266, 29);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(112, 173, 132, 29);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(324, 70, 132, 29);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(324, 161, 30, 50);
		contentPane.add(lblTipo);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(364, 173, 114, 29);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		chckbxCantidadDeEntregas = new JCheckBox("Cantidad de entregas");
		chckbxCantidadDeEntregas.setEnabled(false);
		chckbxCantidadDeEntregas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCantidadDeEntregas.setBounds(51, 231, 150, 23);
		contentPane.add(chckbxCantidadDeEntregas);

		chckbxCubriTurno = new JCheckBox("Cubri\u00F3 turno");
		chckbxCubriTurno.setEnabled(false);
		chckbxCubriTurno.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCubriTurno.setBounds(257, 231, 97, 23);
		contentPane.add(chckbxCubriTurno);

		spinner = new JSpinner();
		spinner.setEnabled(false);
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		spinner.setBounds(73, 261, 114, 29);
		contentPane.add(spinner);

		btnOk = new JButton("Eliminar");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String NumEmp;
				String fecha;

				NumEmp = "";
				fecha = "";

				NumEmp = textField.getText();
				fecha = textField_3.getText();

				if (JOptionPane.showConfirmDialog(rootPane,
						"Se eliminar� el registro, �desea continuar?",
						"Eliminar Registro", JOptionPane.WARNING_MESSAGE,
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					EliminarMov(NumEmp, fecha);
					limpiar();
				}
			}
		});
		btnOk.setEnabled(false);
		btnOk.setBounds(369, 311, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(468, 311, 89, 23);
		contentPane.add(btnCancel);

		comboBox = new JComboBox();
		comboBox.setEnabled(false);
		comboBox.setBounds(257, 261, 97, 29);
		contentPane.add(comboBox);

		obtenerFecha();

		btnBuscar_1 = new JButton("Buscar");
		btnBuscar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String numEmp = "";
				String fechaBusqueda = "";
				contFech = 0;

				numEmp = textField.getText();
				fechaBusqueda = textField_3.getText();

				if (numEmp.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir numero de empleado");
				} else if (fechaBusqueda.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir fecha de busqueda");
					textField_3.setText(fechasistema);
				} else if (fechaBusqueda.compareTo(fechasistema) > 0) {
					JOptionPane
							.showMessageDialog(null,
									"La fecha a ingresar no debe ser mayor al dia actual");
				} else {
					consFecha(numEmp, fechaBusqueda);
					if (contFech == 0) {
						JOptionPane.showMessageDialog(null,
								"No existe ningun registro de entrega con el numero de empleado "
										+ numEmp);
						limpiar();
						inicializarVar();
					} else {
						obtenerInfEmp(numEmp);
						contAce++;
						if (contAce > 1) {
							limpiar();
						}
					}
				}
				textField_1.setText(nombre);

				if (chofer == 1) {
					textField_2.setText("Chofer");
				}
				if (cargador == 1) {
					textField_2.setText("Cargador");
				}
				if (auxiliar == 1) {
					textField_2.setText("Auxiliar");
				}
				if (tipoInt == 1) {
					textField_4.setText("Interno");
				}
				if (tipoExt == 1) {
					textField_4.setText("Externo");
				}

				if (cubrioTurno == 1) {
					chckbxCubriTurno.setSelected(true);
					comboBox.addItem(turnoCub);
					comboBox.setVisible(true);
				} else if (cubrioTurno == 0) {
					chckbxCubriTurno.setSelected(false);
					comboBox.setVisible(false);
				}

				if (cantEntregas != 0)
					chckbxCantidadDeEntregas.setSelected(true);
				spinner.setValue(cantEntregas);

			}
		});

		btnBuscar_1.setBounds(466, 71, 89, 25);
		contentPane.add(btnBuscar_1);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 106, 531, 194);
		contentPane.add(scrollPane_1);

	}

	public void consFecha(String numEmp, String fecha) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		try {
			resultado = conexion
					.getQuery("select cubrioTurno,turnoCubierto,cantidadEntregas from capmovimientos where fecha="
							+ "\""
							+ fecha
							+ "\""
							+ " and numeroEmp="
							+ numEmp
							+ "");

			while (resultado.next()) {

				cubrioTurno = resultado.getInt("cubrioTurno");
				turnoCub = resultado.getString("turnoCubierto");
				cantEntregas = resultado.getInt("cantidadEntregas");

				contFech++;

			}

			// System.out.println(contEmp);
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar los movimientos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	public void obtenerInfEmp(String numemp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		contFech = 0;

		try {
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");
			while (resultado.next()) {

				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++;
			}
			btnOk.setEnabled(true);
			// System.out.println(contEmp);
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void limpiar() {
		textField.setText("");
		textField_1.setText("");
		textField_2.setText("");
		textField_4.setText("");
		chckbxCubriTurno.setSelected(false);
		comboBox.setVisible(false);
		chckbxCantidadDeEntregas.setSelected(false);
		spinner.setValue(0);
	}

	public void inicializarVar() {
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		cubrioTurno = 0;
		turnoCub = "";
		cantEntregas = 0;
		btnOk.setEnabled(false);

	}

	public void obtenerFecha() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		formateador.format(ahora);
		fechasistema = formateador.format(ahora);
		textField_3.setText(formateador.format(ahora));
	}

	public void EliminarMov(String numeroEmp, String fechaSis) {
		MyDataAcces conexion = new MyDataAcces();
		try {
			conexion.setQuery("DELETE FROM `capmovimientos` WHERE `fecha`="
					+ "\"" + fechaSis + "\"" + " and `numeroEmp`=" + numeroEmp
					+ "");

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error : Al eliminar movimientos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
