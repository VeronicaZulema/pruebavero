package SistemaNomina;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuscarMovimientos extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;
	private JLabel lblNmeroDeEmpleado;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblFecha;
	private JLabel lblTipo;
	private JCheckBox chckbxCantidadDeEntregas;
	private JCheckBox chckbxCubriTurno;
	private JSpinner spinner;
	private JButton btnOk;
	private JButton btnCancel;
	private JComboBox comboBox;
	private JScrollPane scrollPane_1;

	// Declaracion de variables publicas los otros metodos pueden tener acceso a
	// ellas
	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contFech;
	public static String fechasistema;
	public static int contAce;
	public static int cubrioTurno = 0;
	public static String turnoCub = "";
	public static int cantEntregas = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarMovimientos frame = new BuscarMovimientos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarMovimientos() {
		setTitle("Busqueda de Movimientos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 584, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CapturaMovimientos BuscarMovimientos = new CapturaMovimientos();
				BuscarMovimientos.setVisible(true);
				dispose();

			}
		});
		btnNuevo.setBounds(27, 24, 83, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setEnabled(false);
		btnBuscar.setBounds(112, 24, 89, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarMovimientos BuscarMovimientos = new ModificarMovimientos();
				BuscarMovimientos.setVisible(true);
				dispose();
			}
		});
		btnModificar.setBounds(204, 24, 89, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarMovimiento BuscarMovimientos = new EliminarMovimiento();
				BuscarMovimientos.setVisible(true);
				dispose();
			}
		});
		btnEliminar.setBounds(295, 24, 83, 23);
		contentPane.add(btnEliminar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 531, 50);
		contentPane.add(scrollPane);

		lblNmeroDeEmpleado = new JLabel("N\u00FAmero de empleado:");
		lblNmeroDeEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmeroDeEmpleado.setBounds(10, 71, 132, 50);
		contentPane.add(lblNmeroDeEmpleado);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(52, 137, 50, 50);
		contentPane.add(lblNombre);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(52, 176, 42, 56);
		contentPane.add(lblRol);

		lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setBounds(264, 71, 47, 50);
		contentPane.add(lblFecha);

		textField = new JTextField();
		textField.setBounds(140, 83, 114, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(112, 149, 219, 29);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(112, 191, 132, 29);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(306, 83, 132, 29);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(338, 179, 30, 50);
		contentPane.add(lblTipo);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(378, 191, 114, 29);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		chckbxCantidadDeEntregas = new JCheckBox("Cantidad de entregas");
		chckbxCantidadDeEntregas.setEnabled(false);
		chckbxCantidadDeEntregas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCantidadDeEntregas.setBounds(62, 239, 150, 23);
		contentPane.add(chckbxCantidadDeEntregas);

		chckbxCubriTurno = new JCheckBox("Cubri\u00F3 turno");
		chckbxCubriTurno.setEnabled(false);
		chckbxCubriTurno.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCubriTurno.setBounds(281, 236, 97, 23);
		contentPane.add(chckbxCubriTurno);

		spinner = new JSpinner();
		spinner.setEnabled(false);
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		spinner.setBounds(84, 262, 114, 29);
		contentPane.add(spinner);

		btnOk = new JButton("Buscar");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String numEmp = "";
				String fechaBusqueda = "";
				contFech = 0;

				numEmp = textField.getText();
				fechaBusqueda = textField_3.getText();

				if (numEmp.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir numero de empleado");
				} else if (fechaBusqueda.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir fecha de busqueda");
					textField_3.setText(fechasistema);
				} else if (fechaBusqueda.compareTo(fechasistema) > 0) {
					JOptionPane
							.showMessageDialog(null,
									"La fecha a ingresar no debe ser mayor al dia actual");
				} else {
					consFecha(numEmp, fechaBusqueda);
					if (contFech == 0) {
						JOptionPane.showMessageDialog(null,
								"No existe ningun registro de entrega con el numero de empleado "
										+ numEmp);
						limpiar();
						inicializarVar();
					} else {
						obtenerInfEmp(numEmp);
						contAce++;
						if (contAce > 1) {
							limpiar();
						}
					}
				}
				textField_1.setText(nombre);

				if (chofer == 1) {
					textField_2.setText("Chofer");
				}
				if (cargador == 1) {
					textField_2.setText("Cargador");
				}
				if (auxiliar == 1) {
					textField_2.setText("Auxiliar");
				}
				if (tipoInt == 1) {
					textField_4.setText("Interno");
				}
				if (tipoExt == 1) {
					textField_4.setText("Externo");
				}

				if (cubrioTurno == 1) {
					chckbxCubriTurno.setSelected(true);
					comboBox.addItem(turnoCub);
					comboBox.setVisible(true);
				} else if (cubrioTurno == 0) {
					chckbxCubriTurno.setSelected(false);
					comboBox.setVisible(false);
				}

				if (cantEntregas != 0)
					chckbxCantidadDeEntregas.setSelected(true);
				spinner.setValue(cantEntregas);
			}
		});
		btnOk.setBounds(369, 311, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(468, 311, 89, 23);
		contentPane.add(btnCancel);

		comboBox = new JComboBox();
		comboBox.setBounds(275, 266, 103, 23);
		contentPane.add(comboBox);
		comboBox.setVisible(false);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 123, 531, 182);
		contentPane.add(scrollPane_1);

		obtenerFecha();

	}

	public void consFecha(String numEmp, String fecha) {
		// Metodo para buscar los movimientos de un empleado y si cubrio turno
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		try {
			resultado = conexion
					.getQuery("select cubrioTurno,turnoCubierto,cantidadEntregas from capmovimientos where fecha="
							+ "\""
							+ fecha
							+ "\""
							+ " and numeroEmp="
							+ numEmp
							+ "");

			while (resultado.next()) {

				cubrioTurno = resultado.getInt("cubrioTurno");
				turnoCub = resultado.getString("turnoCubierto");
				cantEntregas = resultado.getInt("cantidadEntregas");

				contFech++; // Si esta variable es > 0 quiere decir que el
							// empleado que se esta consultando si tiene
							// movimientos en esa fecha, si es 0 notiene
							// movimientos en esa fecha y se valida para que
							// muestre un mensaje.

			}

			// System.out.println(contEmp);
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar los movimientos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	public void obtenerInfEmp(String numemp) {
		//Metodo donde se obtiene la informacion del empleado
		MyDataAcces conexion = new MyDataAcces(); //Instancia la clase donde se encuentra la conexion para la BD.
		ResultSet resultado;  //Variable para obtener los resultados de la consulta.

		try {
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");
			while (resultado.next()) {
				
				//Almacena los valores que trajo la consulta en las variables.
				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++; //Variable para saber si existe el empleado que se desea buscar.
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void obtenerFecha() {
		//Obtiene la fecha del dia actual y la pone en el campo fecha del formulario.
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		formateador.format(ahora);
		fechasistema = formateador.format(ahora);
		textField_3.setText(formateador.format(ahora));
	}

	public void limpiar() {
		//Metodo para limpiar los cuadros de texto.
		textField.setText("");
		textField_1.setText("");
		textField_2.setText("");
		textField_4.setText("");
		chckbxCubriTurno.setSelected(false);
		comboBox.setVisible(false);
		chckbxCantidadDeEntregas.setSelected(false);
		spinner.setValue(0);
	}

	public void inicializarVar() {
		//Metodo para inicializar las variables.
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		cubrioTurno = 0;
		turnoCub = "";
		cantEntregas = 0;

	}

}
