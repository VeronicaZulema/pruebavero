package SistemaNomina;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class MyDataAcces {

	private String _usuario = "root";
	private String _pwd = "";
	private static String _bd = "nominaempleados";
	static String _url = "jdbc:mysql://localhost/" + _bd;
	private Connection conn = null;

	public MyDataAcces() {

		try {
			Class.forName("com.mysql.jdbc.Connection");
			conn = (Connection) DriverManager.getConnection(_url, _usuario,
					_pwd);
			if (conn != null) {
				System.out.println("Conexion a base de datos " + _url
						+ " . . . Ok");
			}
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null,
					"Error : Al conectarse a la base de datos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		} catch (ClassNotFoundException ex) {
			System.out.println(ex);
		}
	}

	public ResultSet getQuery(String _query) {
		Statement state = null;
		ResultSet resultado = null;
		try {
			state = (Statement) conn.createStatement();
			resultado = state.executeQuery(_query);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error : Al hacer la consulta",
					"Mensaje", JOptionPane.ERROR_MESSAGE);

		}
		return resultado;
	}

	public void setQuery(String _query) {

		Statement state = null;

		try {
			state = (Statement) conn.createStatement();
			state.execute(_query);

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error : Al hacer la consulta",
					"Mensaje", JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void main(String[] args) {
		MyDataAcces conexion = new MyDataAcces();

	}
}