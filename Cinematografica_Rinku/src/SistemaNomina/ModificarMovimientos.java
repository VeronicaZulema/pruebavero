package SistemaNomina;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ModificarMovimientos extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;

	private JLabel lblNmeroDeEmpleado;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblFecha;
	private JLabel lblTipo;

	private JCheckBox chckbxCantidadDeEntregas;
	private JCheckBox chckbxCubriTurno;

	private JSpinner spinner;

	private JButton btnOk;
	private JButton btnCancel;
	private JButton btnBuscar_1;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contFech;
	public static String fechasistema;
	public static int cubrioTurno = 0;
	public static String turnoCub = "";
	public static int cantEntregas = 0;
	private JComboBox comboBox;
	private JScrollPane scrollPane_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModificarMovimientos frame = new ModificarMovimientos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModificarMovimientos() {
		setTitle("Modificar Movimientos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 584, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CapturaMovimientos ModificarMovimientos = new CapturaMovimientos();
				ModificarMovimientos.setVisible(true);
				dispose();
	
			}
		});
		btnNuevo.setBounds(27, 24, 83, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarMovimientos ModificarMovimientos = new BuscarMovimientos();
				ModificarMovimientos.setVisible(true);
				dispose();
				
			}
		});
		btnBuscar.setBounds(112, 24, 89, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.setEnabled(false);
		btnModificar.setBounds(204, 24, 89, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarMovimiento ModificarMovimientos = new EliminarMovimiento();
				ModificarMovimientos.setVisible(true);
				dispose();
			}
		});
		btnEliminar.setBounds(295, 24, 83, 23);
		contentPane.add(btnEliminar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 531, 50);
		contentPane.add(scrollPane);

		lblNmeroDeEmpleado = new JLabel("N\u00FAmero de empleado:");
		lblNmeroDeEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmeroDeEmpleado.setBounds(27, 71, 132, 50);
		contentPane.add(lblNmeroDeEmpleado);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(52, 132, 50, 50);
		contentPane.add(lblNombre);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(52, 170, 42, 56);
		contentPane.add(lblRol);

		lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setBounds(281, 71, 47, 50);
		contentPane.add(lblFecha);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					e.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField.setBounds(157, 83, 114, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(112, 144, 219, 29);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(112, 185, 132, 29);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(321, 83, 132, 29);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(321, 173, 30, 50);
		contentPane.add(lblTipo);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(361, 185, 114, 29);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		chckbxCantidadDeEntregas = new JCheckBox("Cantidad de entregas");
		chckbxCantidadDeEntregas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chckbxCantidadDeEntregas.isSelected() == (true)) {
					spinner.setEnabled(true);
				} else {
					spinner.setEnabled(false);
					// spinner.setValue(0);

				}
			}
		});
		chckbxCantidadDeEntregas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCantidadDeEntregas.setBounds(52, 239, 150, 23);
		contentPane.add(chckbxCantidadDeEntregas);

		chckbxCubriTurno = new JCheckBox("Cubri\u00F3 turno");
		chckbxCubriTurno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chckbxCubriTurno.isSelected() == (true)) {
					comboBox.setVisible(true);
				} else {
					comboBox.setVisible(false);
					// comboBox.setSelectedItem(0);
				}
			}
		});
		chckbxCubriTurno.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCubriTurno.setBounds(254, 239, 97, 23);
		contentPane.add(chckbxCubriTurno);

		spinner = new JSpinner();
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		spinner.setBounds(71, 269, 114, 29);
		contentPane.add(spinner);

		btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int entregasNuevas = 0;
				String numeroEmp = "";
				String fechaSis = "";
				int cubTurno = 0;
				String turnoCub = "";
				int cantEnt = 0;

				numeroEmp = textField.getText();
				fechaSis = textField_3.getText();
				entregasNuevas = (Integer) spinner.getValue();

				// metodo para validar que el empleado que se le quiere agregar
				// un movimimiento exista en la tabla
				if (numeroEmp.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir numero de empleado");
				} else {
					obtenerInfEmp(numeroEmp);
				}

				if (fechaSis.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir fecha de busqueda");
					textField_3.setText(fechasistema);
				} else if (contEmp == 0) {
					limpiar();
					JOptionPane.showMessageDialog(null,
							"El numero de empleado no existe");
				} else if (entregasNuevas <= 0) {
					JOptionPane.showMessageDialog(null,
							"Ingresar cantidad de entregas valida");
				} else {

					if (chckbxCubriTurno.isSelected() == (true)) {
						cubTurno = 1;
					} else {
						cubTurno = 0;
					}

					turnoCub = (String) comboBox.getSelectedItem();
					cantEnt = (Integer) spinner.getValue();

					if (chckbxCubriTurno.isSelected() == (false)) {
						turnoCub = "";
					}
					actualizarMov(numeroEmp, fechaSis, cubTurno, turnoCub,
							cantEnt);

				}
			}
		});
		btnOk.setBounds(369, 311, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(468, 311, 89, 23);
		contentPane.add(btnCancel);

		obtenerFecha();

		btnBuscar_1 = new JButton("Buscar");
		btnBuscar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String numEmp = "";
				String fechaBusqueda = "";

				numEmp = textField.getText();
				fechaBusqueda = textField_3.getText();

				if (numEmp.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir numero de empleado");
				} else if (fechaBusqueda.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir fecha de busqueda");
					textField_3.setText(fechasistema);
				} else if (fechaBusqueda.compareTo(fechasistema) > 0) {
					JOptionPane
							.showMessageDialog(null,
									"La fecha a ingresar no debe ser mayor al dia actual");
				} else {
					consFecha(numEmp, fechaBusqueda);
					if (contFech == 0) {
						JOptionPane.showMessageDialog(null,
								"No existe ningun registro de entrega con el numero de empleado "
										+ numEmp);
						limpiar();
						inicializarVar();
					} else {
						obtenerInfEmp(numEmp);

					}
				}
				textField_1.setText(nombre);

				if (chofer == 1) {
					textField_2.setText("Chofer");
					chckbxCubriTurno.setEnabled(false);
				}
				if (cargador == 1) {
					textField_2.setText("Cargador");
					chckbxCubriTurno.setEnabled(false);
				}
				if (auxiliar == 1) {
					textField_2.setText("Auxiliar");
					chckbxCubriTurno.setEnabled(true);
				}
				if (tipoInt == 1) {
					textField_4.setText("Interno");
				}
				if (tipoExt == 1) {
					textField_4.setText("Externo");
				}
				if (cubrioTurno == 1) {
					chckbxCubriTurno.setSelected(true);
					comboBox.setSelectedItem(turnoCub);
					comboBox.setVisible(true);
				} else if (cubrioTurno == 0) {
					chckbxCubriTurno.setSelected(false);
					comboBox.setVisible(false);
				}
				if (cantEntregas != 0)
					chckbxCantidadDeEntregas.setSelected(true);
				spinner.setValue(cantEntregas);

			}

		});

		btnBuscar_1.setBounds(463, 83, 89, 26);
		contentPane.add(btnBuscar_1);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Chofer",
				"Cargador" }));
		comboBox.setBounds(250, 269, 101, 29);
		contentPane.add(comboBox);
		comboBox.setVisible(false);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 119, 531, 189);
		contentPane.add(scrollPane_1);

	}

	public void consFecha(String numEmp, String fecha) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		contFech = 0;
		try {
			resultado = conexion
					.getQuery("select cubrioTurno,turnoCubierto,cantidadEntregas from capmovimientos where fecha="
							+ "\""
							+ fecha
							+ "\""
							+ " and numeroEmp="
							+ numEmp
							+ "");

			while (resultado.next()) {

				cubrioTurno = resultado.getInt("cubrioTurno");
				turnoCub = resultado.getString("turnoCubierto");
				cantEntregas = resultado.getInt("cantidadEntregas");

				contFech++;

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar los movimientos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void obtenerInfEmp(String numemp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		contEmp = 0;

		try {
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");
			while (resultado.next()) {

				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++;
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void actualizarMov(String numeroEmp, String fechaSis, int cubTurno,
			String turnoCub, int cantEnt) {
		// TODO Auto-generated method stub
		MyDataAcces conexion = new MyDataAcces();// Se instancia la clase de la
													// conexion para conectarse
													// a la base de datos
		try {
			conexion.setQuery("UPDATE `capmovimientos` SET `cubrioTurno`='"
					+ cubTurno + "',`turnoCubierto`='" + turnoCub
					+ "',`cantidadEntregas`='" + cantEnt + "' WHERE `fecha`="
					+ "\"" + fechaSis + "\"" + " and `numeroEmp`=" + numeroEmp
					+ "");

			JOptionPane.showMessageDialog(null,
					"Se modifico el movimiento con fecha " + fechaSis
							+ " para el empleado " + numeroEmp);
			limpiar();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error : Al actualizar los datos del empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void obtenerFecha() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		formateador.format(ahora);
		fechasistema = formateador.format(ahora);
		textField_3.setText(formateador.format(ahora));
	}

	public void limpiar() {
		textField_1.setText("");
		textField_2.setText("");
		textField_4.setText("");
		chckbxCubriTurno.setSelected(false);
		comboBox.setVisible(false);
		chckbxCantidadDeEntregas.setSelected(false);
		spinner.setValue(0);

	}

	public void inicializarVar() {
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		cubrioTurno = 0;
		turnoCub = "";
		cantEntregas = 0;

	}
}
