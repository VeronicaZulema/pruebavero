package SistemaNomina;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JScrollPane;

public class NominaEmpleados extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JLabel lblNombre;
	private JLabel lblNumero;
	private JLabel lblSueldoBase;
	private JLabel lblNewLabel;
	private JLabel lblRetencionIsr;
	private JButton btnAceptar;
	private JLabel lblMesAConsultar;
	private JComboBox comboBox;
	private JLabel lblDespensa;
	private JLabel lblSueldoTotal;
	private JTextField textField_9;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	private JLabel lblRol;
	private JTextField textField_7;
	private JLabel lblTipo;
	private JTextField textField_8;
	private JScrollPane scrollPane;
	public static String fechasistema;
	public static int sumaEnt;
	public static int cantEnt;
	public static int entregasfinal;
	public static float totBono;
	public static float sueldoEmp;
	public static int cubrioturnoChofer;
	public static int cubrioturnoCarg;
	public static float bonoAuxChofer;
	public static float bonoAuxCarg;
	public static double totDespensa;
	public static int contMov;
	public static String A�o;
	public static String Mes;
	public static int contAce;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NominaEmpleados frame = new NominaEmpleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NominaEmpleados() {
		setTitle("Nomina");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 567, 389);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(26, 71, 67, 14);
		contentPane.add(lblNombre);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					arg0.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField.setBounds(80, 26, 128, 28);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNumero = new JLabel("Numero:");
		lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNumero.setBounds(26, 25, 67, 28);
		contentPane.add(lblNumero);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(80, 65, 259, 28);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		lblSueldoBase = new JLabel("Sueldo Base:");
		lblSueldoBase.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSueldoBase.setBounds(26, 175, 113, 28);
		contentPane.add(lblSueldoBase);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(110, 176, 113, 28);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		lblNewLabel = new JLabel("Bonos:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(54, 229, 97, 21);
		contentPane.add(lblNewLabel);

		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(110, 226, 113, 28);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(340, 176, 113, 28);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		lblRetencionIsr = new JLabel("Retencion ISR:");
		lblRetencionIsr.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRetencionIsr.setBounds(244, 180, 86, 18);
		contentPane.add(lblRetencionIsr);
		
		contAce=0;

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String NumEmp = "";
				String mesCons = "";
				int index;
				String fechaIni = "";
				String fechaFin = "";
				int cantEntregas = 0;
				float total = 0;
				float totaldes = 0;
				float bonoAux = 0;
				float isr = 0;
				float SueldoFinal = 0;
				
				contAce++;
				
				if(contAce>1){
					//limpiar();
					inicializarVar();
				}
				

				NumEmp = textField.getText();
				mesCons = (String) comboBox.getSelectedItem();
				index = comboBox.getSelectedIndex();

				obtenerFecha();

				if (NumEmp.length() == 0) {
					JOptionPane.showMessageDialog(null,
							"Introducir numero de empleado");
				} else if (mesCons.equals("Seleccionar...")) {
					JOptionPane.showMessageDialog(null,
							"Selecciona el mes a consultar");
				} else {
					buscarEmp(NumEmp);
					if (contEmp == 0) {
						JOptionPane.showMessageDialog(null,
								"No existe el empleado");
						limpiar();
					} else {
						textField_1.setText(nombre);

						if (chofer == 1) {
							textField_7.setText("Chofer");
						}
						if (cargador == 1) {
							textField_7.setText("Cargador");
						}
						if (auxiliar == 1) {
							textField_7.setText("Auxiliar");
						}
						if (tipoInt == 1) {
							textField_8.setText("Interno");
						}
						if (tipoExt == 1) {
							textField_8.setText("Externo");
						}

						if (index == 1) {
							fechaIni = A�o+"-01-01";
							fechaFin = A�o+"-01-30";
						}
						if (index == 2) {
							fechaIni = A�o+"-02-01";
							fechaFin = A�o+"-02-30";
						}
						if (index == 3) {
							fechaIni = A�o+"-03-01";
							fechaFin = A�o+"-03-30";
						}
						if (index == 4) {
							fechaIni = A�o+"-04-01";
							fechaFin = A�o+"-04-30";
						}
						if (index == 5) {
							fechaIni = A�o+"-05-01";
							fechaFin = A�o+"-05-30";
						}
						if (index == 6) {
							fechaIni = A�o+"-06-01";
							fechaFin = A�o+"-06-30";
						}
						if (index == 7) {
							fechaIni = A�o+"-07-01";
							fechaFin = A�o+"-07-30";
						}
						if (index == 8) {
							fechaIni = A�o+"-08-01";
							fechaFin = A�o+"-08-30";
						}
						if (index == 9) {
							fechaIni = A�o+"-09-01";
							fechaFin = A�o+"-09-30";
						}
						if (index == 10) {
							fechaIni = A�o+"-10-01";
							fechaFin = A�o+"-10-30";
						}
						if (index == 11) {
							fechaIni = A�o+"-11-01";
							fechaFin = A�o+"-11-30";
						}
						if (index == 12) {
							fechaIni = A�o+"-12-01";
							fechaFin = A�o+"-12-30";
						}

						obtEntregas(fechaIni, fechaFin, NumEmp);

							cantEntregas = entregasfinal;
							textField_9.setText(Integer.toString(cantEntregas));
							ObtSueldos(cantEntregas);
							String Rol = textField_7.getText();

							if (Rol.equals("Chofer") || Rol.equals("Cargador")) {
								ObtValorBono(Rol);
								total = sueldoEmp + totBono;
								String ext = textField_8.getText();

								if (ext.equals("Interno")) {
									ObtDespensa(total);
									totaldes = (float) (total + totDespensa);

									if (totaldes <= 16000) {
										isr = (float) (totaldes * 0.09);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}
									if (totaldes > 16000) {
										isr = (float) (totaldes * 0.12);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}
									SueldoFinal = totaldes - isr;

									String sueldobase = String
											.valueOf(sueldoEmp);
									String bono = String.valueOf(totBono);
									String desp = String.valueOf(totDespensa);
									String RetISR = String.valueOf(isr);
									String SueldoPer = String
											.valueOf(SueldoFinal);

									textField_2.setText(sueldobase);
									textField_3.setText(bono);
									textField_5.setText(desp);
									textField_4.setText(RetISR);
									textField_6.setText(SueldoPer);

								}

								if (ext.equals("Externo")) {

									if (total <= 16000) {
										isr = (float) (total * 0.09);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}
									if (total > 16000) {
										isr = (float) (total * 0.12);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}

									SueldoFinal = total - isr;

									String sueldobase = String
											.valueOf(sueldoEmp);
									String bono = String.valueOf(totBono);
									String RetISR = String.valueOf(isr);
									String SueldoPer = String
											.valueOf(SueldoFinal);

									textField_2.setText(sueldobase);
									textField_3.setText(bono);
									textField_4.setText(RetISR);
									textField_6.setText(SueldoPer);

								}

							}
							if (Rol.equals("Auxiliar")) {

								auxiliarTurnoChofer(fechaIni, fechaFin, NumEmp);
								auxiliarTurnoCargador(fechaIni, fechaFin,
										NumEmp);

								ObtSueldosAuxChofer(cubrioturnoChofer);
								ObtSueldosAuxCarg(cubrioturnoCarg);

								total = sueldoEmp + bonoAuxChofer + bonoAuxCarg;
								bonoAux = bonoAuxChofer + bonoAuxCarg;

								String ext = textField_8.getText();

								if (ext.equals("Interno")) {

									ObtDespensa(total);
									totaldes = (float) (total + totDespensa);
									if (totaldes <= 16000) {
										isr = (float) (totaldes * 0.09);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}
									if (totaldes > 16000) {
										isr = (float) (totaldes * 0.12);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}

									SueldoFinal = totaldes - isr;

									String sueldobase = String
											.valueOf(sueldoEmp);
									String bonoaux = String.valueOf(bonoAux);
									String desp = String.valueOf(totDespensa);
									String RetISR = String.valueOf(isr);
									String SueldoPer = String
											.valueOf(SueldoFinal);

									textField_2.setText(sueldobase);
									textField_3.setText(bonoaux);
									textField_5.setText(desp);
									textField_4.setText(RetISR);
									textField_6.setText(SueldoPer);

								}
								if (ext.equals("Externo")) {

									if (total <= 16000) {
										isr = (float) (total * 0.09);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}
									if (total > 16000) {
										isr = (float) (total * 0.12);
										String isrEmp = String.valueOf(isr);
										textField_4.setText(isrEmp);
									}

									SueldoFinal = total - isr;

									String sueldobase = String
											.valueOf(sueldoEmp);
									String bonoaux = String.valueOf(bonoAux);
									String RetISR = String.valueOf(isr);
									String SueldoPer = String
											.valueOf(SueldoFinal);

									textField_2.setText(sueldobase);
									textField_3.setText(bonoaux);
									textField_4.setText(RetISR);
									textField_6.setText(SueldoPer);
									

								}

							}

					}
				}

			}
		});
		btnAceptar.setBounds(328, 289, 106, 28);
		contentPane.add(btnAceptar);

		lblMesAConsultar = new JLabel("Mes a Consultar:");
		lblMesAConsultar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMesAConsultar.setBounds(277, 29, 104, 21);
		contentPane.add(lblMesAConsultar);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {
				"Seleccionar...", "Enero", "Febrero", "Marzo", "Abril", "Mayo",
				"Junio", "Julio", "Agosto", "Septiembre", "Octubre",
				"Noviembre", "Diciembre" }));
		comboBox.setBounds(391, 26, 119, 28);
		contentPane.add(comboBox);

		lblDespensa = new JLabel("Despensa:");
		lblDespensa.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDespensa.setBounds(37, 268, 78, 35);
		contentPane.add(lblDespensa);

		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setBounds(110, 272, 113, 28);
		contentPane.add(textField_5);
		textField_5.setColumns(10);

		lblSueldoTotal = new JLabel("Sueldo Total:");
		lblSueldoTotal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSueldoTotal.setBounds(253, 225, 86, 28);
		contentPane.add(lblSueldoTotal);

		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setBounds(340, 226, 113, 28);
		contentPane.add(textField_6);
		textField_6.setColumns(10);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(54, 118, 46, 14);
		contentPane.add(lblRol);

		textField_7 = new JTextField();
		textField_7.setEditable(false);
		textField_7.setBounds(80, 112, 128, 28);
		contentPane.add(textField_7);
		textField_7.setColumns(10);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(239, 118, 46, 14);
		contentPane.add(lblTipo);

		textField_8 = new JTextField();
		textField_8.setEditable(false);
		textField_8.setBounds(277, 112, 113, 28);
		contentPane.add(textField_8);
		textField_8.setColumns(10);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 531, 144);
		contentPane.add(scrollPane);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(452, 289, 89, 28);
		contentPane.add(btnCancel);
		
		JLabel lblEntregas = new JLabel("Entregas:");
		lblEntregas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEntregas.setBounds(37, 305, 78, 35);
		contentPane.add(lblEntregas);
		
		textField_9 = new JTextField();
		textField_9.setEditable(false);
		textField_9.setBounds(110, 314, 113, 26);
		contentPane.add(textField_9);
		textField_9.setColumns(10);
	}

	private void buscarEmp(String numemp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		contEmp = 0;

		try {
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");

			while (resultado.next()) {

				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++;
			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void ObtSueldos(int sumaEnt) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		try {
			resultado = conexion
					.getQuery("select (((sueldoBase * hrsTrabajadas)* diasMes) + ("
							+ sumaEnt
							+ "*entregaCliente)) AS sueldo from sueldos");

			while (resultado.next()) {

				sueldoEmp = resultado.getInt("sueldo");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el sueldo base", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void obtenerFecha() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		formateador.format(ahora);
		fechasistema = formateador.format(ahora);
		A�o = fechasistema.substring(0, 4);
		Mes = fechasistema.substring(5, 7);
	}

	private void obtEntregas(String fechaIni, String fechaFin, String NumEmp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		try {
			resultado = conexion
					.getQuery("select cantidadEntregas from capmovimientos where fecha>="
							+ "\""
							+ fechaIni
							+ "\""
							+ "and fecha<="
							+ "\""
							+ fechaFin + "\"" + " and NumeroEmp=" + NumEmp + "");

			while (resultado.next()) {

				sumaEnt = sumaEnt + resultado.getInt("cantidadEntregas");
				contMov++;

			}

			entregasfinal = sumaEnt;

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al obtener las entregas del empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void ObtValorBono(String Rol) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado = null;

		try {

			if (Rol.equals("Chofer")) {
				resultado = conexion
						.getQuery("select ((bonoHoraChofer * hrsTrabajadas) * diasMes) AS bonoChofer from sueldos");
			} else if (Rol.equals("Cargador")) {
				resultado = conexion
						.getQuery("select ((bonoHoraCargadores * hrsTrabajadas) * diasMes) AS bonoChofer from sueldos");
			}

			while (resultado.next()) {

				totBono = resultado.getInt("bonoChofer");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el bono del empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void auxiliarTurnoChofer(String fechaIni, String fechaFin,
			String NumEmp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		cubrioturnoChofer = 0;
		String turnoChofer = "Chofer";

		try {
			resultado = conexion
					.getQuery("select COUNT(*) AS contChofer from capmovimientos where fecha>="
							+ "\""
							+ fechaIni
							+ "\""
							+ "and fecha<="
							+ "\""
							+ fechaFin
							+ "\""
							+ " and NumeroEmp="
							+ NumEmp
							+ " and turnoCubierto=" + "\"" + turnoChofer + "\"");

			while (resultado.next()) {

				cubrioturnoChofer = resultado.getInt("contChofer");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al obtener las entregas que hizo el empleado",
					"Mensaje", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void auxiliarTurnoCargador(String fechaIni, String fechaFin,
			String NumEmp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		String turnoCarg = "Cargador";
		cubrioturnoCarg = 0;

		try {
			resultado = conexion
					.getQuery("select COUNT(*) AS contCarg from capmovimientos where fecha>="
							+ "\""
							+ fechaIni
							+ "\""
							+ "and fecha<="
							+ "\""
							+ fechaFin
							+ "\""
							+ " and NumeroEmp="
							+ NumEmp
							+ " and turnoCubierto=" + "\"" + turnoCarg + "\"");

			while (resultado.next()) {

				cubrioturnoCarg = resultado.getInt("contCarg");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al obtener las entregas que hizo el empleado",
					"Mensaje", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void ObtSueldosAuxChofer(int cubrioturnoChofer) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		bonoAuxChofer = 0;

		try {
			resultado = conexion
					.getQuery("select ((bonoHoraChofer * hrsTrabajadas) * "
							+ cubrioturnoChofer
							+ ") AS bonoChoferAux from sueldos");

			while (resultado.next()) {

				bonoAuxChofer = resultado.getInt("bonoChoferAux");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane
					.showMessageDialog(
							null,
							"Error : Al consultar el bono del auxiliar cubriendo turno de chofer",
							"Mensaje", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void ObtSueldosAuxCarg(int cubrioturnoCarg) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		bonoAuxCarg = 0;

		try {
			resultado = conexion
					.getQuery("select ((bonoHoraCargadores * hrsTrabajadas) *  "
							+ cubrioturnoCarg
							+ ") AS bonoCargador from sueldos");

			while (resultado.next()) {

				bonoAuxCarg = resultado.getInt("bonoCargador");

			}

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane
					.showMessageDialog(
							null,
							"Error : Al consultar el bono del auxiliar cubriendo turno de cargador",
							"Mensaje", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void ObtDespensa(float total) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		totDespensa = 0;

		try {
			resultado = conexion.getQuery("select (despensa * " + total
					+ ") AS despensa from sueldos");

			while (resultado.next()) {

				totDespensa = resultado.getDouble("despensa");

			}

			System.out.println("Despensa" + totDespensa);

		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al obtener la despensa", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	public void limpiar() {
		textField.setText("");
		textField_1.setText("");
		textField_7.setText("");
		textField_8.setText("");
		textField_2.setText("");
		textField_4.setText("");
		textField_3.setText("");
		textField_5.setText("");
		textField_6.setText("");
		textField_9.setText("");

	}
	
	public void inicializarVar(){
		nombre="";
		chofer=0;
		cargador=0;
		auxiliar=0;
		tipoInt=0;
		tipoExt=0;
		contEmp=0;
		fechasistema="";
		sumaEnt=0;
		cantEnt=0;
		entregasfinal=0;
		totBono=0;
		sueldoEmp=0;
		cubrioturnoChofer=0;
		cubrioturnoCarg=0;
		bonoAuxChofer=0;
		bonoAuxCarg=0;
		totDespensa=0;
		contMov=0;
		A�o="";
		Mes="";
	
	}
}
