package SistemaNomina;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Modificar_empleados extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;
	private JLabel lblNmero;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblTipo;
	private JRadioButton rdbtnChofer;
	private JRadioButton rdbtnCargador;
	private JRadioButton rdbtnAuxiliar;
	private JRadioButton rdbtnInterno;
	private JRadioButton rdbtnExterno;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JButton btnBuscar_1;
	private JButton btnModificar_1;
	private JButton btnCancelar;
	private JScrollPane scrollPane_3;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contAce;

	public ButtonGroup agrupadorRol;// Agrupador para los RadioButton
	public ButtonGroup agrupadorTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Modificar_empleados frame = new Modificar_empleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Modificar_empleados() {
		setTitle("Modificar empleado");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 476, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Empleados Modificar_empleados = new Empleados();
				Modificar_empleados.setVisible(true);
				dispose();
			}
		});
		btnNuevo.setBounds(27, 22, 70, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarEmpleados Modificar_empleados = new BuscarEmpleados();
				Modificar_empleados.setVisible(true);
				dispose();
			}
		});
		btnBuscar.setBounds(96, 22, 83, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.setEnabled(false);
		btnModificar.setBounds(178, 22, 98, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarEmpleados Modificar_empleados = new EliminarEmpleados();
				Modificar_empleados.setVisible(true);
				dispose();
			}
		});
		btnEliminar.setBounds(275, 22, 89, 23);
		contentPane.add(btnEliminar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 428, 50);
		contentPane.add(scrollPane);

		lblNmero = new JLabel("N\u00FAmero:");
		lblNmero.setBounds(27, 72, 63, 39);
		lblNmero.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(lblNmero);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					arg0.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField.setBounds(78, 82, 101, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(27, 102, 63, 39);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(lblNombre);

		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (textField_1.getText().length() >= 50)
					e.consume();
				if (Character.isDigit(c)) {
					getToolkit().beep();
					e.consume();
					JOptionPane.showMessageDialog(null, "Ingresar solo letras",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		textField_1.setEnabled(false);
		textField_1.setBounds(78, 113, 319, 28);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		lblRol = new JLabel("Rol:");
		lblRol.setBounds(48, 157, 63, 39);
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(lblRol);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(232, 157, 63, 39);
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(lblTipo);

		rdbtnChofer = new JRadioButton("Chofer");
		rdbtnChofer.setEnabled(false);
		rdbtnChofer.setBounds(78, 166, 109, 23);
		contentPane.add(rdbtnChofer);

		rdbtnCargador = new JRadioButton("Cargador");
		rdbtnCargador.setEnabled(false);
		rdbtnCargador.setBounds(78, 192, 109, 23);
		contentPane.add(rdbtnCargador);

		rdbtnAuxiliar = new JRadioButton("Auxiliar");
		rdbtnAuxiliar.setEnabled(false);
		rdbtnAuxiliar.setBounds(78, 218, 109, 23);
		contentPane.add(rdbtnAuxiliar);

		rdbtnInterno = new JRadioButton("Interno");
		rdbtnInterno.setEnabled(false);
		rdbtnInterno.setBounds(268, 166, 109, 23);
		contentPane.add(rdbtnInterno);

		rdbtnExterno = new JRadioButton("Externo");
		rdbtnExterno.setEnabled(false);
		rdbtnExterno.setBounds(268, 192, 109, 23);
		contentPane.add(rdbtnExterno);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 146, 166, 112);
		contentPane.add(scrollPane_1);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(213, 146, 182, 112);
		contentPane.add(scrollPane_2);

		agrupador();

		btnBuscar_1 = new JButton("Buscar");
		btnBuscar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String numemp;

				numemp = "";
				contEmp = 0;

				contAce++;
				if (contAce > 1) {
					limpiar();
				}

				numemp = textField.getText();
				buscarEmp(numemp);

				if (contEmp == 0) {
					JOptionPane.showMessageDialog(null,
							"No existe ningun empleado registrado");
					desactivarCom();
					inicializarvar();
					limpiar();

				}

				textField_1.setText(nombre);

				if (chofer == 1) {
					rdbtnChofer.setSelected(true);
				}
				if (cargador == 1) {
					rdbtnCargador.setSelected(true);
				}
				if (auxiliar == 1) {
					rdbtnAuxiliar.setSelected(true);
				}
				if (tipoInt == 1) {
					rdbtnInterno.setSelected(true);
				}
				if (tipoExt == 1) {
					rdbtnExterno.setSelected(true);
				}

				if (nombre.length() != 0) {
					abilitarcom();
				}
			}
		});
		btnBuscar_1.setBounds(187, 81, 89, 30);
		contentPane.add(btnBuscar_1);

		btnModificar_1 = new JButton("Modificar");
		btnModificar_1.setEnabled(false);
		btnModificar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nombreEmpN;
				int choferAlmN;
				int choferCargN;
				int choferAuxN;
				int tipoIntN;
				int tipoExtN;
				String numEmp;

				nombreEmpN = "";
				choferAlmN = 0;
				choferCargN = 0;
				choferAuxN = 0;
				tipoIntN = 0;
				tipoExtN = 0;
				numEmp = "";

				numEmp = textField.getText();
				nombreEmpN = textField_1.getText();

				if (rdbtnChofer.isSelected() == (true)) {
					choferAlmN = 1;
				} else if (rdbtnCargador.isSelected() == (true)) {
					choferCargN = 1;
				} else if (rdbtnAuxiliar.isSelected() == (true)) {
					choferAuxN = 1;
				}

				if (rdbtnInterno.isSelected() == (true)) {
					tipoIntN = 1;
				} else if (rdbtnExterno.isSelected() == (true)) {
					tipoExtN = 1;
				}

				if (nombre.equals(nombreEmpN) && chofer == choferAlmN
						&& cargador == choferCargN && auxiliar == choferAuxN
						&& tipoInt == tipoIntN && tipoExt == tipoExtN) {
					JOptionPane.showMessageDialog(null,
							"No se modifico ningun dato");
				} else {
					actualizarEmp(numEmp, nombreEmpN, choferAlmN, choferCargN,
							choferAuxN, tipoIntN, tipoExtN);
				}

			}
		});
		btnModificar_1.setBounds(268, 294, 89, 29);
		contentPane.add(btnModificar_1);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(361, 294, 89, 29);
		contentPane.add(btnCancelar);

		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 72, 428, 200);
		contentPane.add(scrollPane_3);

	}

	private void buscarEmp(String numemp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		resultado = conexion
				.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
						+ numemp + "");

		try {
			while (resultado.next()) {

				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");
				contEmp++;

			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			a.printStackTrace();
		}
	}

	private void actualizarEmp(String numEmp, String nombreEmpN,
			int choferAlmN, int choferCargN, int choferAuxN, int tipoIntN,
			int tipoExtN) {
		// TODO Auto-generated method stub
		try {
			MyDataAcces conexion = new MyDataAcces();// Se instancia la clase de
														// la conexion para
														// conectarse a la base
														// de datos

			conexion.setQuery("UPDATE `empleados` SET `Nombre`='" + nombreEmpN
					+ "',`Chofer`='" + choferAlmN + "',`Cargador`='"
					+ choferCargN + "',`Auxiliar`='" + choferAuxN
					+ "',`tipoInt`='" + tipoIntN + "',`tipoExt`='" + tipoExtN
					+ "' WHERE  `Numero`='" + numEmp + "'");
			JOptionPane.showMessageDialog(null, "Se mofico el empleado "
					+ numEmp);
			limpiar();
			inicializarvar();
			desactivarCom();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error : Al actualizar los datos del empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void limpiar() {

		textField_1.setText("");

		agrupadorRol.remove(rdbtnChofer);
		agrupadorRol.remove(rdbtnCargador);
		agrupadorRol.remove(rdbtnAuxiliar);

		agrupadorTipo.remove(rdbtnExterno);
		agrupadorTipo.remove(rdbtnInterno);

		rdbtnChofer.setSelected(false);
		rdbtnCargador.setSelected(false);
		rdbtnAuxiliar.setSelected(false);
		rdbtnInterno.setSelected(false);
		rdbtnExterno.setSelected(false);

		agrupador();

	}

	public void agrupador() {

		agrupadorRol = new ButtonGroup();
		agrupadorRol.add(rdbtnChofer);
		agrupadorRol.add(rdbtnCargador);
		agrupadorRol.add(rdbtnAuxiliar);

		agrupadorTipo = new ButtonGroup();
		agrupadorTipo.add(rdbtnInterno);
		agrupadorTipo.add(rdbtnExterno);

	}

	public void abilitarcom() {
		textField_1.setEnabled(true);
		rdbtnChofer.setEnabled(true);
		rdbtnCargador.setEnabled(true);
		rdbtnAuxiliar.setEnabled(true);
		rdbtnInterno.setEnabled(true);
		rdbtnExterno.setEnabled(true);
		btnModificar_1.setEnabled(true);
	}

	public void inicializarvar() {
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		contEmp = 0;
		contAce = 0;

	}

	public void desactivarCom() {
		textField_1.setEnabled(false);
		rdbtnChofer.setEnabled(false);
		rdbtnCargador.setEnabled(false);
		rdbtnAuxiliar.setEnabled(false);
		rdbtnInterno.setEnabled(false);
		rdbtnExterno.setEnabled(false);
		btnModificar_1.setEnabled(false);

	}

}
