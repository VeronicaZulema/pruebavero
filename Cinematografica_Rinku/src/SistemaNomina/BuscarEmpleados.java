package SistemaNomina;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BuscarEmpleados extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JLabel lblNmero;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JRadioButton rdbtnChofer;
	private JRadioButton rdbtnCargador;
	private JRadioButton rdbtnAuxiliar;
	private JLabel lblTipo;
	private JRadioButton rdbtnInterno;
	private JRadioButton rdbtnExterno;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JScrollPane scrollPane_3;
	private JButton btnOk;
	private JButton btnCancel;
	private JScrollPane scrollPane;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contAce;

	private ButtonGroup agrupadorRol;// Agrupador para los RadioButton
	private ButtonGroup agrupadorTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarEmpleados frame = new BuscarEmpleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarEmpleados() {
		setTitle("Busqueda de empleados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 474, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Empleados BuscarEmpleados = new Empleados();
				BuscarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnNuevo.setBounds(27, 22, 70, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setEnabled(false);
		btnBuscar.setBounds(96, 22, 83, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modificar_empleados BuscarEmpleados = new Modificar_empleados();
				BuscarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnModificar.setBounds(178, 22, 98, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarEmpleados BuscarEmpleados = new EliminarEmpleados();
				BuscarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnEliminar.setBounds(275, 22, 89, 23);
		contentPane.add(btnEliminar);

		lblNmero = new JLabel("N\u00FAmero:");
		lblNmero.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmero.setBounds(27, 72, 63, 39);
		contentPane.add(lblNmero);

		contAce = 0;

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					arg0.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField.setBounds(78, 82, 101, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(27, 102, 63, 39);
		contentPane.add(lblNombre);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(78, 113, 319, 28);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(48, 157, 63, 39);
		contentPane.add(lblRol);

		rdbtnChofer = new JRadioButton("Chofer");
		rdbtnChofer.setEnabled(false);
		rdbtnChofer.setBounds(78, 166, 109, 23);
		contentPane.add(rdbtnChofer);

		rdbtnCargador = new JRadioButton("Cargador");
		rdbtnCargador.setEnabled(false);
		rdbtnCargador.setBounds(78, 192, 109, 23);
		contentPane.add(rdbtnCargador);

		rdbtnAuxiliar = new JRadioButton("Auxiliar");
		rdbtnAuxiliar.setEnabled(false);
		rdbtnAuxiliar.setBounds(78, 218, 109, 23);
		contentPane.add(rdbtnAuxiliar);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(232, 157, 63, 39);
		contentPane.add(lblTipo);

		rdbtnInterno = new JRadioButton("Interno");
		rdbtnInterno.setEnabled(false);
		rdbtnInterno.setBounds(268, 166, 109, 23);
		contentPane.add(rdbtnInterno);

		rdbtnExterno = new JRadioButton("Externo");
		rdbtnExterno.setEnabled(false);
		rdbtnExterno.setBounds(268, 192, 109, 23);
		contentPane.add(rdbtnExterno);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 146, 166, 112);
		contentPane.add(scrollPane_1);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(213, 146, 182, 112);
		contentPane.add(scrollPane_2);

		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 72, 428, 200);
		contentPane.add(scrollPane_3);

		agrupador();

		btnOk = new JButton("Buscar");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numemp;

				numemp = "";
				contEmp = 0;

				contAce++;
				if (contAce > 1) {
					limpiar();
				}

				numemp = textField.getText();
				buscarEmp(numemp);

				if (contEmp == 0) {
					JOptionPane.showMessageDialog(null,
							"Empleado no registrado");
					inicializarvar();
					limpiar();
				}

				textField_1.setText(nombre);

				if (chofer == 1) {
					rdbtnChofer.setSelected(true);
				}
				if (cargador == 1) {
					rdbtnCargador.setSelected(true);
				}
				if (auxiliar == 1) {
					rdbtnAuxiliar.setSelected(true);
				}
				if (tipoInt == 1) {
					rdbtnInterno.setSelected(true);
				}
				if (tipoExt == 1) {
					rdbtnExterno.setSelected(true);
				}
			}
		});
		btnOk.setBounds(250, 297, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancel.setBounds(349, 297, 89, 23);
		contentPane.add(btnCancel);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 428, 50);
		contentPane.add(scrollPane);
	}

	private void buscarEmp(String numemp) {
		//Metodo para buscar el empleado en la base de datos
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		try {
			//Select con los campos que se deben de buscar filtrando por el numero de empleado.
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");

			while (resultado.next()) {
				//Variables donde se almacenaran los valores que trae el select.
				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++; //Si el select trae informacion del empleado el contEmp aumenta, sino trae el contEmp queda en 0.
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void limpiar() {
		//Metodo para limpiar los radiobutton

		textField_1.setText("");

		agrupadorRol.remove(rdbtnChofer);
		agrupadorRol.remove(rdbtnCargador);
		agrupadorRol.remove(rdbtnAuxiliar);

		agrupadorTipo.remove(rdbtnExterno);
		agrupadorTipo.remove(rdbtnInterno);

		rdbtnChofer.setSelected(false);
		rdbtnCargador.setSelected(false);
		rdbtnAuxiliar.setSelected(false);
		rdbtnInterno.setSelected(false);
		rdbtnExterno.setSelected(false);

		agrupador();

	}

	public void agrupador() {
		//Metodo para agrupar los radiobutton

		agrupadorRol = new ButtonGroup();
		agrupadorRol.add(rdbtnChofer);
		agrupadorRol.add(rdbtnCargador);
		agrupadorRol.add(rdbtnAuxiliar);

		agrupadorTipo = new ButtonGroup();
		agrupadorTipo.add(rdbtnInterno);
		agrupadorTipo.add(rdbtnExterno);

	}

	public void inicializarvar() {
		//Metodo para inicializar variables
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		contEmp = 0;
		contAce = 0;

	}

}
